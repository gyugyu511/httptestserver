var fs = require('fs');

var reader_json_loader = {};


reader_json_loader.init = function(app, config, callback)
{
    console.log('Reader_Json.init()');
    readjson(app, config, callback);
}

var readjson = function (app, config, callback)
{
    var utddata = {};
    
    var dataLength = config.jsondata_info.length;
    console.log('설정에 JsonData 의 수 : %d', dataLength);
    var count = 0;
    adddata(app, config, count, dataLength, utddata, callback);
}

var adddata = function(app, config, count, dataLength, utddata, callback){
    var dir = __dirname + config.jsondata_info[count].file;
    fs.readFile(dir, 'utf8', function (err, data) {
        if (err) {
            console.log('Error: ' + err);
            return;
        }

        var curData = config.jsondata_info[count];

        obj_pulseconfig = JSON.parse(data);
        utddata[curData.method] = obj_pulseconfig;
        //console.dir(obj_pulseconfig);
        
        console.log('JSON 데이터 [%s] 추가됨.', curData.method);
        if(++count >= dataLength){
            onCompleted(app, utddata, callback);
        }else{
            adddata(app, config, count, dataLength, utddata, callback);
        }
    });
}


var onCompleted = function(app, utddata, callback)
{
    app.set('utddata', utddata);
    //console.dir(utddata);
    callback();
}

module.exports = reader_json_loader;