module.exports = {
    server_port : 4000,
    //db_url : 'mongodb://test123:123@ds149613.mlab.com:49613/utdgamedb',
    db_url : 'mongodb://localhost:27017/local',
    db_schemas: [
        {file:'./user_schema', collection:'users', schemaName: 'UserSchema', modelName: 'UserModel'}
        ,{file:'./character_schema', collection:'characters', schemaName: 'CharacterSchema', modelName: 'CharacterModel'}
    ],
    route_info: [
	    //===== User =====//
	    {file:'./user', path:'/process/login', method:'login', type:'post'}
        ,{file:'./character', path:'/process/character/create', method:'createCharacter', type:'post'}			
        ,{file:'./character', path:'/process/character/select', method:'selectCharacter', type:'post'}			
        ,{file:'./game_start', path:'/process/ingame/startgame', method:'startgame', type:'post'}
        ,{file:'./game_clear', path:'/process/ingame/gameclear', method:'gameclear', type:'post'}
        ,{file:'./item', path:'/process/getitems', method:'getitems', type:'get'}
        ,{file:'./stationery', path:'/process/stationery/sale', method:'saleItem', type:'post'}
        ,{file:'./shop_spirit', path:'/process/shop_spirit/buy', method:'buyItem', type:'post'}
        ,{file:'./shop_laundry', path:'/process/shop_laundry/buy', method:'buyClothes', type:'post'}
        ,{file:'./change_clothes', path:'/process/clothes/change', method:'change_clothes', type:'post'}
        ,{file:'./change_clothes', path:'/process/clothes/remove', method:'remove_clothes', type:'post'}  
	],
    jsondata_info: [
	    {file:'/items.json', method:'item'}
        ,{file:'/level.json', method:'level'}
        ,{file:'/common.json', method:'common'}
        ,{file:'/clothes.json', method:'clothes'}
        ,{file:'/shop_spirit.json', method:'shop_spirit'}
        ,{file:'/shop_laundry.json', method:'shop_laundry'}
        ,{file:'/map.json', method:'map'}
        ,{file:'/quest.json', method:'quest'}
	],
    helper_info:[
        {file:'./item_helper', method:'itemHelper'},
        {file:'./helper', method:'helper'}
    ]
}