var saleItem = function(req, res){
    
    var database = req.app.get('database');
    if(database.db){
        
        console.dir(req.body);
        var userid = req.body.userid;
        var cid    = req.body.cid;
        var itemID = req.body.itemid;
        var itemkey = req.body.itemkey;
        var itemcount = req.body.count;

        var utddata = req.app.get('utddata');
        
        // 1개당 금액.
        var eachgold = utddata.item[itemID].gold;

        var resdata = {};
        
        // 아이템을 찾아서 갱신함.
        var selectOption = {"items": {$elemMatch : {"key": itemkey}}};
        database.CharacterModel.findOneAndUpdate({"_id": cid, "items.key": itemkey},{$inc: { "items.$.count": -itemcount }},{"upsert": true, "select" : selectOption, "new" : true}, function(err, result){
            
            if(err){
                 console.log('아이템 DB 갱신 실패. KEY :['+ itemkey +']/ ERR :'+err.stack);
            }
   
            console.log('아이템 갱신');
            var remainCount = result.items[0]._doc.count;
            resdata.key = itemkey;

            // 다 팔아버림.
            if(remainCount == 0){
                console.log('다 팔아버림.');
                //DB에서 삭제함.
                removeItem(database, cid, itemkey, function(success){
                    if(success == true){
                        resdata.isdelete = true;
                        updateGold(database, userid, eachgold, itemcount, res, resdata);
                    }
                });
            }else{
                resdata.isdelete = false;
                resdata.count = remainCount;
                console.log('몇개만 팜. : ' + remainCount);
                updateGold(database, userid, eachgold, itemcount, res, resdata);

            }

        });
    }
}

var removeItem = function(database, cid, itemkey, callback){
    database.CharacterModel.findOneAndUpdate({"_id": cid, "items.key": itemkey}, {$pull:{ "items":{"key" : itemkey} }}, function(err, reuslt){
        if(err){
            console.log('removeItem 아이템 삭제 실패. KEY :['+ itemkey +']/ ERR :'+err.stack);
            callback(false);
            return;
        }
            
        callback(true);
    });
}

var updateGold = function(database, userid, eachgold, count, res, resdata){
    
    var addgold = eachgold * count;
    
    // 돈을 찾아요.
    var query = {"userid":userid};
    database.UserModel.findOneAndUpdate(query, {$inc: { "gold": addgold }}, {new : true}, function(err, result){
            if(err){
                console.log('updateGold 유저 GOLD 갱신 실패. USERID :['+ userid +']+/ ERR :'+err.stack);
            }else{
                resdata.gold = result.gold;
                onSend_saleItem(res, resdata);
            }
    });
    
}

var onSend_saleItem = function(res, data)
{
    var resJsonString = JSON.stringify(data);
    res.write(resJsonString);
    res.end();
}

module.exports.saleItem = saleItem;