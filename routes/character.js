// character 생성. path : /process/character/create
var createCharacter = function(req, res) 
{
    console.log('createCharacter 호출됨.');
    // 데이터베이스 객체 참조
	var database = req.app.get('database');
    
    console.dir(req.body);
    var userID = req.body.userid;
    var nickname = req.body.nickname;
    var chartype = req.body.chartype;
    
   // 데이터베이스 객체가 초기화된 경우.
	if (database.db) {
    
        // CharacterModel 인스턴스 생성
	   var character = new database.CharacterModel({"userid":userID, "nickname":nickname, "chartype":chartype});

        // save()로 저장    
	   character.save(function(err) {
		if (err) {
            console.log(err);
			return;
		}
  
       // 사용자 DB에 갱신합니다.
       database.UserModel.findOneAndUpdate({"userid":userID},{$push: {characters:nickname}}, function(err, results){
            if(err){
                console.err('사용자 DB characters 업뎃 실패' +err.stack);
            }
        });
           
	   console.log("캐릭터 데이터 추가함.");
       onSend_createCharacter(res, true);
	   });
    }
}

var onSend_createCharacter = function(res, success){
    var resjson = {};
    resjson.success = success;
  
    console.log(resjson);
    var resJsonString = JSON.stringify(resjson);
    res.write(resJsonString);
    //resjson["success"] = success;
    //res.write(resjson);
    res.end();
}

var selectCharacter = function(req, res){
    console.log('selectCharacter 호출됨.');
        
    // 데이터베이스 객체 참조
	var database = req.app.get('database');
    var userID = req.body.userid;
    var nickname = req.body.nickname;
    
    var query = {"userid":userID, 'characters':{$in : [userID]}};
    
    // DB에 userid도 있고, characters에 nickname도 있는지 찾고 lastcharcter 갱신함.
    database.UserModel.findOneAndUpdate(query, {$set: {lastcharacter:nickname}}, function(err, doc) {
        
        if(err){
            console.err('사용자 DB lastcharacter 업뎃 실패' +err.stack);
            return;
        }
       
        var charquery = {"nickname":nickname};
        database.CharacterModel.findByNickname(nickname, function(err, doc){
            
            if(err){
                console.err('캐릭터 DB 못찾음.' +err.stack);
                return;
            }
            onSend_selectCharacter(res, doc.toObject());
        });
    
   });
}

var onSend_selectCharacter = function(res, doc)
{
     // id를 cid로 변경하고, 삭제함.
    doc.cid = doc._id;
 
    delete doc._id;
    delete doc.__v;
    delete doc.created_at;
    delete doc.items;
    
    var resdata = {};
    resdata = doc;
    resdata.equipedclothes = [];
    
    for(var i = 0; i < doc.clothes.length; i++){
            var clothes = doc.clothes[i];
            if(clothes.id.length > 0){
                resdata.equipedclothes.push(clothes.id);
            }
        }
    
  
    delete doc.clothes;

    console.dir(resdata);
    
    var resJsonString = JSON.stringify(resdata);
    res.write(resJsonString);
    res.end();
}



module.exports.createCharacter = createCharacter;
module.exports.selectCharacter = selectCharacter;