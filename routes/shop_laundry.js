var buyClothes = function(res, req){
    
    var database = req.app.get('database');
    if(database.db == false ) return;
    
    var resdata = {};
    resdata.items = [];
    var userID  = res.body.userid;
    var cid     = res.body.cid;
    var clothesID = req.body.clothesid;
    
    var table = req.app.get('utddata').shop_laundry[clothesID];
    if(table.length == 0) return;
    
    
    var needGold = table.gold;
    var needMaterial_id = table.material_id;
    var needMaterial_count = table.material_count;
    var materialMax = table.material_id.length;
        
    var userQuery = {"userid": userID};
    database.UserModel.findOne(userQuery, function(err, userDoc){
       
        if(err){
            console.log('유저 DB 검색 실패. ERR : ' + err.stack);
            return;
        }
        
        if(userDoc.gold < needGold){
            // 골드 부족.
            return;
        }
        
        var charQuery = {"_id": cid};
        database.CharacterModel.findOne(charQuery, function(err, characterDoc){
           if(err){
                console.log('캐릭터 DB 검색 실패. ERR : ' + err.stack);
                return;
            }
            
            characterDoc.isOwnedClothes(clothesID, function(isExist){
                
                if(isExist == true){
                    // 이미 있는 옷입니다... 잘못 사셨어영.
                    return;
                }
                
                for(var i = 0 ; i < materialMax; i++){
            
                    characterDoc.isCheckItem(needMaterial_id[i], needMaterial_count[i], function(isEnough){
                        if(isEnough == false){
                            // 아이템 모자름요.
                            return;
                        }
                    });
                }
            
                // 실제로 데이터를 반영할 시간입니다.
                userDoc.updateGold(-needGold, function(updateGoldUserDoc){
                    if(updateGoldUserDoc.success == false){
                        // 골드 DB 실패.
                        return;
                    }
                
                    resdata.gold = updateGoldUserDoc.gold;
                
                    // 아이템 DB 갱신을 합시다.
                    var itemHelper = req.app.get('helpers').itemHelper;
                    var helper = req.app.get('helpers').helper;
                    var completedCount = 0;
                    var isError = false;
                    
                
                    for(var i = 0 ; i < materialMax; i++){
                
                        itemHelper.removeItem(database, cid, needMaterial_id[i], needMaterial_count[i], function(resultData){
                        
                            if(resultData.success == false){
                                // 아이템 DB 갱신 실패임. 에러코드는 resultData.errorcode에 있음.
                                isError = true;
                            }   
                        
                            resdata.items.push(resultData);
                        
                            if(++completedCount == materialMax){
                                //아이템 DB 갱신 끝남.
                            
                                if(isError == true){
                                    // 에러가 있었심.
                                    return;
                                }
                            
                                //데이터 넣어줘야함.
                                characterDoc.addOwnedClothes(clothesID, function(addOwnedClothesDoc){
                                    if(addOwnedClothesDoc.success == false){
                                        // 아이템 추가.. 실패함. 완전 ... 아이템은 다 제거하고.. 옷은 지급 못함..
                                        return;
                                    }
                                        
                                    resdata.clothesid = clothesID;
                                    helper.onSend_Json(resdata);
                                });
                            }
                        });
                    }
                });
            });
        });
    });
}


module.exports.buyClothes = buyClothes;