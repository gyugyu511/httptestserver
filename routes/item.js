var getitems = function(req, res){
    
    var database = req.app.get('database');
    var cid = req.query.cid;
    console.log('getitems 호출 :' + cid);
    
    if(database.db){
        var query = {"_id" : cid};
         database.CharacterModel.findOne(query, function(err, doc){
            var jsondata ={};
            jsondata.items = [];
            if(err){
                console.err('캐릭터 DB 못찾음.' +err.stack);
                return;
            }
            
             doc.items.forEach(function(v,i){
                 delete v._doc._id;
                 jsondata.items.push(v);
             });
               var resJsonString = JSON.stringify(jsondata);
                res.write(resJsonString);
                res.end();
        });
    }
    
    //console.log('getitems 호출 :' +req.query.nickname);
}

var test = function(req, res){
    //res.end();
}
module.exports.getitems = getitems;
