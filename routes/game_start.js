var startgame = function(req, res){
    console.log('startgame 호출함.');
    var database = req.app.get('database');
    
    // 데이터베이스 객체가 초기화된 경우.
	if (database.db) {
        var userID = req.body.userid;

        
        var resdata = {};
        
        database.UserModel.findOne({"userid": userID}, function(err, userResult){
            if(err){
                console.err('유저 DB 못찾음.' +err.stack);
                return;
            }
            
            if(userResult._doc.ticket.amount < 1){
                // 티켓이 모자라요.
            }
            else{
                // 티켓 차감.
                userResult.consumeTicket(function(ticketResultDoc){
                    updateTicket(resdata, ticketResultDoc);
                    
                    //맵&퀘스트 세팅.
                    onSetMapID(res, req, database, resdata);
                    
                    //hppotion 차감.
                    
                });
            }
        });
            
            
    }
}

var updateTicket = function(resdata, doc){
    resdata.ticket = {};
    resdata.ticket.amount       = doc.ticket.amount;
    resdata.ticket.max          = doc.ticket.max;
    resdata.ticket.updatetime   = doc.ticket.update_at.getTime()/1000;
}

var onSetMapID = function(res, req, database, resdata){
    
    var mapData = {};
    var mapTable = req.app.get('utddata').map;
    
    // 아이템 안썼으니까 랜덤으로 보낼거임.
    //입장권 씀.
    
    mapData = mapTable[0];
    resdata.mapid = mapData.mapid;
    
    onSetQuest(req, resdata, mapTable, mapData);
    //onSetItems(req, database, resdata, mapTable, mapData);
    
    onComsumeItems(req, database, resdata, function(resdata){
        onSend_gameStart(res, resdata);
    });
}

var onSetQuest = function(req, resdata, mapTable, mapData){
    resdata.quests = [];
    var quest = {};
    
    var helper = req.app.get('helpers').helper;
    var randomIndex = helper.getRandomIntInclusive(0, mapData.questList.length-1);
    //console.log('randomIndex %d',randomIndex);
    var questID = mapData.questList[randomIndex];
    var questData = req.app.get('utddata').quest[questID];
    //console.log(questID);
    //console.dir(mapData.questList);
    var count   = helper.getRandomIntInclusive(questData.min, questData.max);
    quest.id = questID;
    quest.count = count;
    
    resdata.quests.push(quest);
}

var onComsumeItems = function(req, database, resdata, callback){
     // items 넣어둘 공간 만들기.
    resdata.items =[];
    
    var items = req.body.consumeitems;
    var itemsCountMax = items.length;
    var cid = req.body.cid;
    var updateCount = 0;
    
    if(itemsCountMax == 0){
        callback(resdata);
        return;
    }
    
     var itemHelper = req.app.get('helpers').itemHelper;
    
    for(var i = 0 ; i <itemsCountMax; i++){
        var itemID = items[i].itemid; 
        var count =  -items[i].count;
         
        itemHelper.addItem(req, database, doc, itemID, count, function(resultDoc){
             
            if(resultDoc != null){
                            
                delete resultDoc._id;
                resdata.items.push(resultDoc);
            }
            
            if(++updateCount >= itemsCountMax){
                callback(resdata);
            }
        });
    
    }
}

var onSetItems = function(req, database, resdata, mapTable, mapData){
    
    resdata.items = [];
    
    var helper = req.app.get('helpers').helper;
    
    // 공용 아이템 리스트임.
    var mapItemList = mapTable[0].mapItemLIst;
    var mapItemListLength = mapItemList.length-1;
    var commonItemCount = helper.getRandomIntInclusive(mapData.itemMin, mapData.itemMax);
    for(var i = 0 ; i < commonItemCount ; i++){
        var item = mapItemList[helper.getRandomIntInclusive(0, mapItemListLength)];
        console.log(item);
        resdata.items.push(item);
    }
}


var onSend_gameStart = function(res, data)
{
    var resJsonString = JSON.stringify(data);
    console.log(data);
    res.write(resJsonString);
    res.end();
}

module.exports.startgame = startgame;