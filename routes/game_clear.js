var gameclear = function(req, res){
    console.log('gameclear 호출함.');
    var database = req.app.get('database');
    //console.dir(req.body.items);
    var userID = req.body.userid;
    var cid = req.body.cid;
    
    //console.dir(req.body);
    
    if (database.db) {
          
        // res로 보낼 데이터 생성.
        var resdata = {};
        updateExp(req, database, resdata, function(resdata, doc){
            updateEtc(req, database, resdata, doc, function(resdata){
                updateItems(req, database, resdata, doc, function(resdata){
                    onSend_gameClaer(res, resdata);
                });
            });
        });
    }
}


var updateExp = function(req, database, resdata, callback){
    
    var cid = req.body.cid;
    database.CharacterModel.findOne({"_id":cid}, function(err, doc){

            if(err){
                console.err('캐릭터 DB 못찾음.' +err.stack);
                return;
            }
        
            var utddata = req.app.get('utddata');
        
            //EXP 처리.
            var addExp = 1200;
            var currentExp = doc.exp;
            resdata.addexp = addExp;
        
            // 캐릭터 타입을 가져와야함.
            // 현재 캐릭터의 레벨의 최대 경험치를 가져왔습니다.
            console.log('레벨:'+doc.level);
            //console.dir(utddata.level[doc.level]);
            var maxExp = utddata.level[doc.level].exp;
            
            var totalExp = currentExp+addExp;
            if(totalExp >= maxExp){
                // 레벨업 합니다.
                var remainExp = totalExp - maxExp;
                var query   = {"_id":cid};
                var update  = {$set:{"exp": remainExp}, $inc:{"level": 1}};
                var options = {"upsert" : true, "new" : true};
                
                database.CharacterModel.findOneAndUpdate(query, update, options, function(err, result){
                    if(err){
                        console.log('캐릭터 레벨업 DB 갱신 실패. ERR : ' + err.stack);
                        return;
                    }
                    
                    console.log('캐릭터 레벨업');
                    //console.log(result);
                    //
                    resdata.levelup = true;
                    resdata.level   = result.level;
                    resdata.exp     = remainExp;
                    
                    // 최대 레벨에 따른 티켓 수정하기.
                    var maxTicket = utddata.level[result.level].ticket;
                    var userQuery ={"userid": result.userid, "ticket.max":{"$lt":maxTicket}};
                    var userUpdate = {$set:{"ticket.max":maxTicket}};
                    var options = {"upsert" : true, "new" : true};
                    database.UserModel.findOneAndUpdate(userQuery, userUpdate,options, function(err, userResult){
                       if(err){
                           console.log('캐릭터 레벨업 ticket.max 갱신 실패 ERR : ' + err.stack);
                        }
                        userResult.refreshTicket(function(ticketResultDoc){
                            //console.dir(ticketResultDoc);
                            updateTicket(resdata, ticketResultDoc);
                            callback(resdata, result);            
                        });
                    });
                });
            }
            else{
                // 레벨업 안함.
                var query = {"_id":cid};
                var update = {$set:{"exp": totalExp}};
                
               database.CharacterModel.findOneAndUpdate(query, update, function(err, result){
                    if(err){
                        console.log('캐릭터 EXP 증가 DB 갱신 실패. ERR : ' + err.stack);
                        return;
                    }
                   
                   console.log('캐릭터 EXP ' + totalExp);
                   //
                   resdata.levelup = false;
                   resdata.level   = result.level;
                   resdata.exp     = totalExp;
                   
                   var userQuery ={"userid": result.userid};
                     database.UserModel.findOne(userQuery, function(err, userResult){
                         if(err){
                            console.log('updateExp 유저 못찾음요.' + err.statck);
                            callback(resdata, result); 
                             return;
                         }
                          userResult.refreshTicket(function(ticketResultDoc){
                            //console.dir(ticketResultDoc);
                           updateTicket(resdata, ticketResultDoc);
                            callback(resdata, result);            
                        });
                     });
                   
                });
            }
        });
}

var updateTicket = function(resdata, doc){
    resdata.ticket_amount       = doc.ticket.amount;
    resdata.ticket_max          = doc.ticket.max;
    resdata.ticket_updatetime   = doc.ticket.update_at.getTime()/1000;
}

var updateEtc = function(req, database, resdata, doc, callback){
    doc.refreshSpirit(req.body.spirit, function(totalSpirit){
        resdata.spirit = totalSpirit;
        callback(resdata); 
    });
}

var updateItems = function(req, database, resdata, doc, callback){
        
    // items 넣어둘 공간 만들기.
    resdata.items =[];
    
    var items           = req.body.items;
    var itemCountMax    = items.length;
    var cid             = req.body.cid;
    var updateCount     = 0;
    
    if(itemCountMax == 0){
        callback(resdata);
        return;
    }
    
    var itemHelper = req.app.get('helpers').itemHelper;
    
    for(var i = 0; i < itemCountMax; i++){
        
        var itemID = items[i].itemid;
        var count =  items[i].count;
        //console.dir(items[i]);

        itemHelper.addItem(req, database, doc, itemID, count, function(resultDoc){
             
            if(resultDoc != null){
                            
                delete resultDoc._id;
                resdata.items.push(resultDoc);
            }
            
            if(++updateCount >= itemCountMax){
                callback(resdata);
            }
        });
    }
}

var onSend_gameClaer = function(res, data)
{
    var resJsonString = JSON.stringify(data);
    //console.log(data);
    res.write(resJsonString);
    res.end();
}

module.exports.gameclear = gameclear;
