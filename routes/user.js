/*
 * 사용자 정보 처리 모듈
 * 데이터베이스 관련 객체들을 req.app.get('database')로 참조
 *
 * @date 2016-11-10
 * @author Mike
 */

// path : /process/login
var login = function(req, res) {
	console.log('login 호출됨.');

    // 데이터베이스 객체 참조
	var database = req.app.get('database');
	var utddata = req.app.get('utddata');
     
    var leveldata = utddata.level;
    var userID = req.body.userid;
    console.dir(req.body);
    
    // 데이터베이스 객체가 초기화된 경우, authUser 함수 호출하여 사용자 인증
	if (database.db) {
		findUser(database, userID, function(err, docs) {
			// 에러 발생 시, 클라이언트로 에러 전송
			if (err) {
                console.error('사용자 로그인 중 에러 발생 : ' + err.stack);
				res.end();
                return;
            }
			
            // 조회된 레코드가 없으면 생성.
			if (!docs) {
				addUser(database, userID, leveldata, function(err, doc){
                    if (err){
                        console.error('사용자 추가 중 에러 발생 : ' + err.stack);
				        res.end();
                        return;
                    }
                    // 사용자를 추가한 데이터를 넘김.
                    onSend_login(res, doc);
                });
			}else{
                docs[0].refreshTicket(function(){
                    // 있던 사용자 데이터를 넘김.
                    var docJson = docs[0].toObject();
                    //console.dir(docJson.ticket.update_at.getTime()/1000);
                    onSend_login(res, docJson);    
                });
                
            }
		});
	} else {  // 데이터베이스 객체가 초기화되지 않은 경우 실패 응답 전송
		res.writeHead('200', {'Content-Type':'text/html;charset=utf8'});
		res.write('<h2>데이터베이스 연결 실패</h2>');
		res.write('<div><p>데이터베이스에 연결하지 못했습니다.</p></div>');
		res.end();
	}
	
};

var findUser = function(database, id, callback){
    // 1. 아이디를 이용해 검색
	database.UserModel.findById(id, function(err, results) {
		if (err) {
			callback(err, null);
			return;
		}
		
		console.log('아이디 [%s]로 사용자 검색결과', id);
		//console.dir(results);
		
		if (results.length > 0) {
			console.log('아이디와 일치하는 사용자 찾음.');
            callback(null, results);
		} else {
	    	console.log("아이디와 일치하는 사용자를 찾지 못함.");
	    	callback(null, null);
	    }
	});
}

//사용자를 등록하는 함수
var addUser = function(database, userid, leveldata, callback) {
	console.log('addUser 호출됨.');
    var maxTicket = leveldata["1"].ticket;
	// UserModel 인스턴스 생성
	var user = new database.UserModel({"userid":userid, "gold":10000, "ruby":500, "ticket.max":maxTicket , "ticket.amount":3});
	//user.characters = [{}];
    
    // TODO : 임시로 userid를 캐릭터 이름으로 정해둠.
    //user.characters[0] = userid;
    
    // save()로 저장    
	user.save(function(err) {
		if (err) {
            console.log('addUser ERROR :'+err);
			callback(err, null);
			return;
		}
		
	    console.log("사용자 데이터 추가함.");
	    callback(null, user);
	});
}


var onSend_login = function(res, doc)
{
    console.log('onSend_login 호출.');
    var resjson = {};
    resjson["userid"] = doc.userid;
    resjson["characters"] = doc.characters;
    resjson["gold"] = doc.gold;
    resjson["ruby"] = doc.ruby;
    resjson["lastcharacter"] = doc.lastcharacter;
    resjson.ticket_amount = doc.ticket.amount;
    resjson.ticket_max = doc.ticket.max;
    resjson.ticket_updatetime = doc.ticket.update_at.getTime()/1000;
    var nowDate = new Date();
    resjson.time = nowDate.getTime()/1000;
    
    //console.dir(resjson);
    var resJsonString = JSON.stringify(resjson);
    res.write(resJsonString);
    res.end();
}

module.exports.login = login;

