var change_clothes = function(req, res){
    var database = req.app.get('database');
    if(database.db == false) return;
    
    var resdata = {};
    var userID  = req.body.userid;
    var cid     = req.body.cid;
    var clothesID = req.body.clothesid;
    
    var helper = req.app.get('helpers').helper;
    var clothesTable = req.app.get('utddata').clothes[clothesID];
     if(clothesTable.length == 0) 
    {
        // 테이블에 ID가 없어요.
        return;
    }
    //console.dir(clothesTable);
    
    var charQuery = {"_id" :cid};
    
    database.CharacterModel.findOne(charQuery, function(err, characterDoc){
       
         if(err){
            console.log('캐릭터 DB 검색 실패. ERR : ' + err.stack);
            return;
        }
        
        characterDoc.isOwnedClothes(clothesID, function(result){
           if(result == false){
               // 소유한 옷이 아님.
               //return;
           }
        
            var enumType = helper.getClothesTypeEnum(clothesTable.type);
            if(enumType == "NONE"){
                // 잘못된 타입임. 이 옷의 ID가 테이블에 없거나. Type명칭이 잘못됨.
                return;
            }
            
            characterDoc.clothes_change(enumType, clothesID, function(result){
                // 공간 만들기.
                resdata.clothes = [];
                var clothes = result.clothes;
                for(var i = 0; i < clothes.length; i++){
                    if(clothes[i].id.length > 0){
                        resdata.clothes.push(clothes[i].id);
                       
                    }
                }
                
                helper.onSend_Json(res, resdata);
            });
        });
    });
};

var remove_clothes = function(req, res){
    
     var database = req.app.get('database');
    if(database.db == false)return;
    
    var resdata = {};
    var userID  = req.body.userid;
    var cid     = req.body.cid;
    var clothesID = req.body.clothesid;
    
    var helper = req.app.get('helpers').helper;
    var clothesTable = req.app.get('utddata').clothes[clothesID];
    if(clothesTable.length == 0) 
    {
        // 테이블에 ID가 없어요.
        return;
    }
    
    //console.dir(clothesTable);
    
    var charQuery = {"_id" :cid};
    
    database.CharacterModel.findOne(charQuery, function(err, characterDoc){
       
         if(err){
            console.log('캐릭터 DB 검색 실패. ERR : ' + err.stack);
            return;
        }
        
    
        
        var enumType = helper.getClothesTypeEnum(clothesTable.type);
        if(enumType == "NONE"){
            // 잘못된 타입임. 이 옷의 ID가 테이블에 없거나. Type명칭이 잘못됨.
            return;
        }

        characterDoc.clothes_change(enumType, '', function(result){
            // 공간 만들기.
            resdata.clothes = [];
            var clothes = result.clothes;
            for(var i = 0; i < clothes.length; i++){
                if(clothes[i].id.length > 0){
                    resdata.clothes.push(clothes[i].id);
                }
            }

            helper.onSend_Json(res, resdata);
        });
        
    });
};

var onSend_error = function(res, data)
{
    var resJsonString = JSON.stringify(data);
    //console.log(data);
    res.write(resJsonString);
    res.end();
}

module.exports.change_clothes = change_clothes;
module.exports.remove_clothes = remove_clothes;