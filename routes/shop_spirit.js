var buyItem = function(req, res){
    
    console.dir(buyItem);
    var database = req.app.get('database');
    if(database.db){
        var resdata = {};
        var userID  = req.body.userid;
        var cid     = req.body.cid;
        var itemID  = req.body.itemid;
        var itemCount = req.body.count;
      
        var shopSpiritData = req.app.get('utddata').shop_spirit;
        console.dir(shopSpiritData);
        var bottleSpiritAmount = req.app.get('utddata').common.bottle_spirit_max.value;
        console.log('bottleSpiritAmount 개수 %d', bottleSpiritAmount);
        var needSpiritAmount = shopSpiritData[itemID].bottleAmount * itemCount * bottleSpiritAmount;
    
        console.log('필요한 개수 %d',needSpiritAmount);
        // 소지한 도깨비 불 병 개수 체크해야됨.
        var userQuery = {"userid": userID};
     
        console.dir(req.body);
        database.UserModel.findOne(userQuery, function(err, result){
             if(err){
                console.log('유저 DB 검색 실패. ERR : ' + err.stack);
                return;
            }
            
            console.log('캐릭터 찾음.');
            if(result.spirit < needSpiritAmount){
                // 도깨비 불이 부족해요.
            }
            else{
                
                var charQuery = {"_id":cid};
                database.CharacterModel.findOne(charQuery, function(err, characterDoc){
                    if(err){
                        console.log('캐릭터 DB 검색 실패. ERR : ' + err.stack);
                        return;
                    }
                    
                    // 도깨비 불이 감소하고, 아이템을 추가합시다.
                    characterDoc.consume_spirit(needSpiritAmount, function(resultDoc){
                        var itemHelper = req.app.get('helpers').itemHelper;
                        resdata.spirit = resultDoc.spirit;
                        itemHelper.addItem(req, database, resultDoc, itemID, itemCount, function(itemResultDoc){
                            console.dir(itemResultDoc);
                            delete itemResultDoc._id;
                            resdata.item = itemResultDoc;
                            onSend_buyItem(res, resdata);
                        });
                    });
                });
            }
        });
    }    
}

var onSend_buyItem = function(res, resdata){
    console.dir(resdata);
    var resJsonString = JSON.stringify(resdata);
    res.write(resJsonString);
    res.end();
}

module.exports.buyItem = buyItem;