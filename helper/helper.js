
// min (포함) 과 max (포함) 사이의 임의 정수를 반환
// Math.round() 를 사용하면 고르지 않은 분포를 얻게된다!
function getRandomIntInclusive(min, max) {
  return Math.floor(Math.random() * (max - min + 1)) + min;
}

function getClothesTypeEnum(typeString){
    
    var clothesEnum = ["NONE", "HAIR", "IN", "OUT", "LEG", "FOOT", "HAT", "GS", "HAND", "MASK", "FACE"];
    
    if(typeString == "Hair") return clothesEnum[1];
    else if(typeString == "Inner") return clothesEnum[2];
    else if(typeString == "Outer") return clothesEnum[3];
    else if(typeString == "Leg") return clothesEnum[4];
    else if(typeString == "Foot") return clothesEnum[5];
    else if(typeString == "Hat") return clothesEnum[6];
    else if(typeString == "Glasses") return clothesEnum[7];
    else if(typeString == "Hand") return clothesEnum[8];
    else if(typeString == "Mask") return clothesEnum[9];
    else if(typeString == "Face") return clothesEnum[10];
    return "NONE";
}

var onSend_Json = function(res, resdata){
    //console.dir(resdata);
    var resJsonString = JSON.stringify(resdata);
    res.write(resJsonString);
    res.end();
}

var onRenderView = function(req, res, view, context){
    res.writeHead('200', {'Contnet-Type':'text/html;charset=utf8'});
    req.app.render(view, context, function(err, html){
        if(err){
           console.log('뷰 렌더링 중 오류 발생' + err.stack);
           res.writeHead('200', {'Contnet-Type':'text/html;charset=utf8'});
           res.write('<h2>뷰 렌더링 중 오류 발생</h2>');
           res.write('<p>' + err.stack + '</p>');
           res.end();

           return;
        } 
        res.end(html);
    });
}

module.exports.getRandomIntInclusive = getRandomIntInclusive;
module.exports.getClothesTypeEnum = getClothesTypeEnum;
module.exports.onSend_Json = onSend_Json;
module.exports.onRenderView = onRenderView;
