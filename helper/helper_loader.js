var helper_loader = {};
helper_loader.init = function(app, config)
{
    console.log('Helper init()');
    addHelper(app, config);
}

var addHelper = function(app, config){
    
    var helpers = {};
    
    var dataLength = config.helper_info.length;
    
    for(var i = 0 ; i < dataLength; i++){
        
        var curData = config.helper_info[i];
        var helper = require(curData.file);
        helpers[curData.method] = helper;
        console.log('%s 모듈을 불러들인 후 Helper 추가함.', curData.file);
        
    }
    
    app.set('helpers', helpers);
}


module.exports = helper_loader;