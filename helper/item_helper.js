var addItem = function(req, database, doc, itemID, count, callback)
{
    // items 넣어둘 공간 만들기.
    var cid     = req.body.cid;
    
    var isExistItem = false;
    if(doc == null){
        var query = {"_id":cid, "items.id" : itemID};
        database.CharacterModel.findOne(query, function(err, result){
           if(err){
               //console.log('캐릭터에 아이템 DB 검색 실패. ID :['+itemID+']/ ERR :'+err.stack);
                isExistItem = false;
           }else{
                isExistItem = true;
            }
            updateItem(isExistItem, req, database, itemID, count, callback);
        });
    }
    else{
        var itemdata = doc.findItemID(itemID);
        isExistItem = itemdata != null;
        updateItem(isExistItem, req, database, itemID, count, callback);
    }
    
    
}
var updateItem = function(isExistItem, req, database, itemID, count, callback){
    
    var cid     = req.body.cid;
    var rand    = req.app.get('randKey');
    
    if(isExistItem == true){
        // 아이템 있음.
        console.log('아이템 있음.');
        
        
        
        var query           = {"_id":cid, "items.id" : itemID};
        var update          = {$inc : { "items.$.count" : count }};
        var selectOption    = {"items" : {$elemMatch : {"id": itemID}}};
        var options         = {"upsert" : true, "select" : selectOption, "new" : true};
        // 기존에 존재하는 아이템 아이디니까 추가하기.           
        database.CharacterModel.findOneAndUpdate(query, update, options, function(err, result){

            if(err)
            {
                console.log('아이템 DB 갱신 실패. ID :['+itemID+']/ ERR :'+err.stack);
                callback(null);
            }
            else
            {
                callback(result.items[0]._doc);
            }
         });
    }else{
        // 아이템이 없었으니까 생성하기.
        console.log('아이템 없음.');
        var query       = {"_id" : cid};
        var itemkey     = rand.generateKey();
        var itemData    = {"key": itemkey, "id": itemID, "count" : count};
        var upate       = {$addToSet : { "items":itemData}};
        var selectOption= {"items" : {$elemMatch : {"id": itemID}}};
        var options         = {"upsert" : true, "select" : selectOption, "new" : true};

        database.CharacterModel.findOneAndUpdate(query, upate, options, function(err, result){
            if(err)
            {
                console.log('아이템 DB 갱신 실패. ID :['+itemID+']/ ERR :'+err.stack);
                  callback(null);
            }
            else
            {
                callback(result.items[0]._doc);
            }
        });
    }
}

var removeItem = function(req, database, itemID, count, callback){

    var cid = req.body.cid;
    
    var resultData = {};
    var selectOption = {"items": {$elemMatch : {"id": itemID}}};

    database.CharacterModel.findOneAndUpdate({"_id": cid, "items.id": itemID},{$inc: { "items.$.count": -count }},{"upsert": true, "select" : selectOption, "new" : true}, function(err, result){
            
            if(err){
                console.log('아이템 DB 갱신 실패. KEY :['+ itemID +']/ ERR :'+err.stack);
                result.success = false;
                result.errorcode = "ERROR_ITEM_DB_FIND";
                return;
            }
   
            //console.log('아이템 갱신');
            var remainCount = result.items[0]._doc.count;
            var itemKey = result.items[0]._doc.key;
        
            resultData.key = itemKey;
            resultData.id = itemID;
        
            // 다 팔아버림.
            if(remainCount == 0){
                console.log('다 팔아버림.');
                
                //DB에서 삭제함.
                removeItemDB(database, cid, itemkey, function(success){
                    if(success == false){
                        result.success = false;
                        result.errorcode = "ERROR_ITEM_DB_REMOVE";
                        return;
                    }
                
                    resultData.count = 0;
                    callback(resultData);
                });
            }else{
                
                resultData.count = remainCount;
                console.log('몇개만 팜. : ' + remainCount);
                
                callback(resultData);
            }
    });
}

var removeItemDB =  function(database, cid, itemkey, callback){
    database.CharacterModel.findOneAndUpdate({"_id": cid, "items.key": itemkey}, {$pull:{ "items":{"key" : itemkey} }}, function(err, reuslt){
        if(err){
            console.log('removeItem 아이템 삭제 실패. KEY :['+ itemkey +']/ ERR :'+err.stack);
            callback(false);
            return;
        }
            
        callback(true);
    });
}

module.exports.addItem = addItem;