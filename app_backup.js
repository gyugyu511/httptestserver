var express = require('express')
    , http = require('http');

// POST로 요청했을 때 요청 파라미터를 확인 할 수 있도록 만들어 둔 body-parser
var bodyParser = require('body-parser');

var router = express.Router();

var app =express();

app.set('port', 8080);

//body-parser를 사용해 application/json 파싱.
app.use(bodyParser.json());

app.use('/s', function(req, res, next){
    var id = req.body.id || req.query.id;
    var pw = req.body.password || req. query.password;
    
    console.log('id : [%s] / pw : [%s]',id,pw);
    res.write('안녕');
    res.end();
});

router.route('/process/login').post(function(req, res){
    console.log('/process/login 호출.');
    
    var id = req.body.id || req.query.id;
    var pw = req.body.password || req. query.password;
    
    //console.log(req);
    console.dir(req.body);
    
    var json = JSON.stringify(req.body);
    res.write(json);
    res.end();
});

app.use('/', router);

http.createServer(app).listen(app.get('port'), function(){
   console.log('서버 시작 IP : %s / Port : %s', getServerIp(), app.get('port'));
});



function getServerIp() {
    var os = require('os');
    var ifaces = os.networkInterfaces();
    var result = '';
    for (var dev in ifaces) {
        var alias = 0;
        ifaces[dev].forEach(function(details) {
            if (details.family == 'IPv4' && details.internal === false) {
                result = details.address;
                ++alias;
            }
        });
    }
    return result;
}