var Schema = {};

Schema.createSchema = function(mongoose){
    
    // 스키마 정의
    var UserSchema = mongoose.Schema({
        userid: {type: String, required: true, unique: true, 'default':''},
        characters: [String],
        gold:{type: Number, 'default': 0},
        ruby:{type: Number, 'default': 0},
        ticket:{
            amount: {type: Number, 'default': 0, min:0 },
            max : {type: Number, 'default': 0, min:0 },
            update_at : {type: Date, index: {unique: false}, 'default': Date.now}
        },
        lastcharacter: {type: String, 'default':''},
        created_at: {type: Date, index: {unique: false}, 'default': Date.now}
    });
    
    // 스키마에 static 메소드 추가
	UserSchema.static('findById', function(id, callback) {
		return this.find({userid:id}, callback);
	});
	
	UserSchema.static('findAll', function(callback) {
		return this.find({}, callback);
	});
    

    UserSchema.method('refreshTicket', function(callback) {
		//console.dir(this);
        var result = this;
        
        var nowDate = new Date();
        if(this.ticket.amount < this.ticket.max)
        {
            var prevDate = this.ticket.update_at.getTime()/1000;
            var amount = Math.floor(((nowDate.getTime()/1000)-prevDate) / 1800);
            if(amount < 1){
                //console.dir(this);
                callback(result);
                return;
            }
            if(this.ticket.amount + amount < this.ticket.max){
                // 작아서 됨. 계속 리젠할거임.
                var regenDate = new Date(prevDate+(amount*1800))*1000;
                this.ticket.update_at = regenDate;
                this.ticket.amount += amount;
            }
            else{
                this.ticket.update_at = nowDate;
                this.ticket.amount = this.ticket.max;
            }
        }else{
            this.ticket.update_at = nowDate;    
        }
        
        
        
        this.save(function(err){
           if(err){
              console.log("ticket 갱신 실패." +err);
            }
            //console.dir(result);
            callback(result);
        });
	});
    
    UserSchema.method('consumeTicket', function(callback){
        var result = this;
        if(this.ticket.amount == this.ticket.max){
            // 원래 다찼던거였으니까.
            // 리젠 체크 시간을 지금으로 설정.
             this.ticket.update_at = new Date();
        }
        
        // 하나 차감.
        this.ticket.amount--;
        
        this.save(function(err){
           if(err){
              console.log("consumeTicket-ticket 갱신 실패." +err);
            }
            //console.dir(result);
            callback(result);
        });
    });
    
    UserSchema.method('updateGold', function(gold, callback){
        var result = this;
        this.gold += gold;
        
        this.save(function(err){
           if(err){
              console.log("updateGold 갱신 실패." +err);
               result.success = false;
            }
            
            result.success = true;
            //console.dir(result);
            callback(result);
        });
        
    });

	console.log('UserSchema 정의함.');

	return UserSchema;
};


module.exports = Schema;
