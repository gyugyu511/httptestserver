var Schema = {};

Schema.createSchema = function(mongoose){
    
    var itemSchema = mongoose.Schema({
        key: {type: String, 'default':''},
        id: {type: String, 'default':''},
        count : {type: Number, 'default': 0, min:0 },
    });

   const CLOTHESTYPE = ["NONE", "HAIR", "IN", "OUT", "LEG", "FOOT", "HAT", "GS", "HAND", "MASK", "FACE"];
    
    
    var clothesSchema = mongoose.Schema({
        type : {type: String, enum : CLOTHESTYPE},
        id:{type: String, 'default':''}
    });
    
    // 스키마 정의
    var CharacterSchema = mongoose.Schema({
        userid: {type: String, required: true, 'default':''},
        nickname : {type : String,  unique: true, required : true, 'deafault':''},
        level: {type: Number, 'default': 1, min : 1},
        exp: {type: Number, 'default': 0, min : 0},
        chartype : {type : String,  required : true, 'default' : ''},
        ownedclothes:[String],
        clothes:{ type:[clothesSchema], requiped: true, 
                 'default' : [{type :"HAIR"},{type :"IN"},{type :"OUT"},{type :"LEG"},{type :"FOOT"},{type :"HAT"},{type :"GS"},{type :"HAND"},{type :"MASK"},{type :"FACE"}]},
        items:[itemSchema],
        skillitems:[String],
        spirit : {type: Number, 'default': 0, min : 0},
        created_at: {type: Date, index: {unique: false}, 'default': Date.now}
    });
    
    // 스키마에 static 메소드 추가
	CharacterSchema.static('findById', function(id, callback) {
		return this.find({userid:id}, callback);
	});
    
    CharacterSchema.static('findByNickname', function(nickname, callback) {
		return this.findOne({nickname:nickname}, callback);
	});
	
	CharacterSchema.static('findAll', function(callback) {
		return this.find({}, callback);
	});
    
    CharacterSchema.method('findItemID', function(id){
      for(var i = 0 ; i < this.items.length; i++){
          var item = this.items[i];
          if(item.id == id){
              return item;
          }
      }
        return null;
    });
    
   CharacterSchema.method('refreshSpirit', function(addSpirit, callback){
       this.spirit += addSpirit;
       var totalspirit = this.spirit;
        this.save(function(err){
           if(err){
              console.log("Spirit 갱신 실패." +err);
            }
            console.dir(totalspirit);
            callback(totalspirit);
        });
    });
    
     // 도깨비 불로 아이템 구매 후.
    CharacterSchema.method('consume_spirit', function(spirit, callback){
       
        this.spirit -= spirit;
         
        var result = this;
        
        this.save(function(err){
           if(err){
              console.log("spirit 갱신 실패." +err);
            }
            //console.dir(result);
            callback(result);
        });
    });
	
    
    CharacterSchema.method('clothes_change', function(type, id, callback){
        var result = this;
        
        for(var i = 0 ; i < this.clothes.length; i++){
            var clothes = this.clothes[i];
            if(clothes.type == type){
               clothes.id = id;
            }
        }

          this.save(function(err){
           if(err){
              console.log("clothes_change 갱신 실패." +err);
            }
            //console.dir(result);
            callback(result);
        });
        
    });
    
    CharacterSchema.method('isOwnedClothes', function(id, callback){
        for(var i = 0 ;i < this.ownedclothes.length; i++){
            if(this.ownedclothes[i] == id){
                callback(true);
                return;
            }
        }
        
        callback(false);
    });
    
    CharacterSchema.method('getClothes', function(callback){
        
        var result = [];
       for(var i = 0; i < this.clothes.length; i++){
            var clothes = this.clothes[i];
            if(clothes.id.length > 0){
                result.push(clothes.id);
            }
        }
        callback(result);
    });
    
	console.log('CharacterSchema 정의함.');

	return CharacterSchema;
};


module.exports = Schema;
