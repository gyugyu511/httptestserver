var express = require('express')
    , http = require('http');

// POST로 요청했을 때 요청 파라미터를 확인 할 수 있도록 만들어 둔 body-parser
var bodyParser = require('body-parser');

// 모듈로 분리한 설정 파일 불러오기
const config = require('./config');

// 모듈로 분리한 데이터베이스 파일 불러오기
const database = require('./database/database');

// 모듈로 분리한 라우팅 파일 불러오기
var route_loader = require('./routes/route_loader');

var reader_json_loader = require('./data/reader_json');

var helper_loader = require('./helper/helper_loader');

var randKey = require("generate-key");

// 익스프레스 객체 생성
var app = express();

var port = process.env.PORT || process.env.OPENSHIFT_NODEJS_PORT || config.server_port;

// body-parser를 사용해 application/json 파싱.
app.use(bodyParser.json());


// 라우팅 정보를 읽어들여 라우팅 설정
route_loader.init(app, express.Router());
app.set('randKey', randKey);
//===== 서버 시작 =====//

// 프로세스 종료 시에 데이터베이스 연결 해제
process.on('SIGTERM', function () {
    console.log("프로세스가 종료됩니다.");
    app.close();
});

app.on('close', function () {
	console.log("Express 서버 객체가 종료됩니다.");
	if (database.db) {
		database.db.close();
	}
});

app.use('/s', function(req, res, next){
    var id = req.body.id || req.query.id;
    var pw = req.body.password || req. query.password;
    
    console.log('id : [%s] / pw : [%s]',id,pw);
    res.write('안녕');
    res.end();
});

/*
router.route('/process/ss').post(function(req, res){
    console.log('/process/login 호출.');
    
    var id = req.body.id || req.query.id;
    var pw = req.body.password || req. query.password;
    
    //console.log(req);
    console.dir(req.body);
    
    var json = JSON.stringify(req.body);
    res.write(json);
    res.end();
});
app.use('/', router);
*/

helper_loader.init(app, config);

http.createServer(app).listen(port, function(){
   console.log('서버 시작 IP : %s / Port : %s', getServerIp(), port);
    
    reader_json_loader.init(app, config, function(){
    // 데이터베이스 초기화
    database.init(app, config);
    });
   
});



function getServerIp() {
    var os = require('os');
    var ifaces = os.networkInterfaces();
    var result = '';
    for (var dev in ifaces) {
        var alias = 0;
        ifaces[dev].forEach(function(details) {
            if (details.family == 'IPv4' && details.internal === false) {
                result = details.address;
                ++alias;
            }
        });
    }
    return result;
}